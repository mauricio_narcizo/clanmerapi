package com.mer.service;

import java.util.Collections;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mer.entity.Membro;
import com.mer.entity.QMembro;
import com.mongodb.MongoClient;
import com.querydsl.mongodb.morphia.MorphiaQuery;

public class MembroService {

	public List<Membro> obterMembros() {
		System.out.println("obterMembros");
		try (MongoClient mongo = new MongoClient()) {
			// MongoDatabase database = mongo.getDatabase("clanmer");
			QMembro membro = new QMembro("Membro");
			Morphia morphia = new Morphia().map(Membro.class);
			Datastore datastore = morphia.createDatastore(mongo, "clanmer");
			MorphiaQuery<Membro> morphiaQuery = new MorphiaQuery<>(morphia, datastore, membro);
			morphiaQuery.where(QMembro.membro.nome.eq("Mauricio 2"));
			List<Membro> list = morphiaQuery.fetch();
			System.out.println(list.size());
			return Collections.emptyList();
		}

	}

	public Membro insertMembro(Membro membro) {
		System.out.println("insert membro");
		try (MongoClient mongo = new MongoClient()) {
			// QMembro qmembro = new QMembro("membro");
			Morphia morphia = new Morphia().map(Membro.class);
			Datastore datastore = morphia.createDatastore(mongo, "clanmer");
			datastore.save(membro);
		}
		return membro;
	}
}
