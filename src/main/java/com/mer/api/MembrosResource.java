package com.mer.api;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mer.entity.Membro;
import com.mer.service.MembroService;

@Path("/membros")
public class MembrosResource {

	@Inject
	MembroService service;

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response obterMembrosClan() {

		// service.obterMembros();

		return Response.ok(service.obterMembros()).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertMembro(Membro membro) {

		return Response.ok(service.insertMembro(membro)).build();
	}
}
